"""
    This algorithm can crop and save the region of interest (roi) using mause
    click to select the roi. Original algorith is taken frompyimagesearch.com
    and edited according to need.

    "r"  button - reset the image to original ones.
    "c"  button - cut and save the roi.
    "p"  button - pass current image without any cropping.
    "q"  button - quit algorithm.
"""
# import the necessary packages
import cv2
import os

# initialize the list of reference points and boolean indicating
# whether cropping is being performed or not
refPt = []
cropping = False

# Saving direction
save_dir = '../capture_img/fixedWing_cropped_img/'

def click_and_crop(event, x, y, flags, param):
    # grab references to the global variables
    global refPt, cropping

    # if the left mouse button was clicked, record the starting
    # (x, y) coordinates and indicate that cropping is being
    # performed
    if event == cv2.EVENT_LBUTTONDOWN:
        refPt = [(x, y)]
        cropping = True

    # check to see if the left mouse button was released
    elif event == cv2.EVENT_LBUTTONUP:
        # record the ending (x, y) coordinates and indicate that
        # the cropping operation is finished
        refPt.append((x, y))
        cropping = False

        # draw a rectangle around the region of interest
        cv2.rectangle(image, refPt[0], refPt[1], (0, 255, 0), 2)
        cv2.imshow("image", image)

for root, dirs, files in os.walk('../fixedWing/'):
    for i, filename in enumerate(files):
        # load the image, clone it, and setup the mouse callback function
        image = cv2.imread(os.path.join(root, filename))
        height, width, depth = image.shape

        ##  Resizing original image if desired un comment it
        # if height > 480 or width > 620:
        #     image = cv2.resize(image,(int(0.5*width), int(0.5*height)))
        #     image = cv2.resize(image,(620, 480))

        clone = image.copy()
        cv2.namedWindow("image")
        cv2.setMouseCallback("image", click_and_crop)

        k = 1

        # keep looping until the 'q' key is pressed
        while True:
        	# display the image and wait for a keypress
            cv2.imshow("image", image)
            key = cv2.waitKey(1) & 0xFF

            # if the 'r' key is pressed, reset the cropping region
            if key == ord("r"):
                image = clone.copy()
                k += 1

            # if the 'c' key is pressed, crop rgion roi and save it.
            elif key == ord("c"):
                # if there are two reference points, then crop the region of interest
                # from teh image and display it
                if len(refPt) == 2:
                    if refPt[0][1] < refPt[1][1]:
                        roi = clone[refPt[0][1]:refPt[1][1], refPt[0][0]:refPt[1][0]]
                        cv2.imshow("ROI", roi)
                        cv2.waitKey(0)
                        cv2.destroyWindow("ROI")
                        # Save image
                        cv2.imwrite(os.path.join(save_dir, 'fixedWing_' + str(k) + str(i+1) + '.png'), roi)

                    elif refPt[0][1] == refPt[1][1] and refPt[0][0] == refPt[1][0]:
                        print("Selected area in not valid try again")

                    else:
                        roi = clone[refPt[1][1]:refPt[0][1], refPt[1][0]:refPt[0][0]]
                        cv2.imshow("ROI", roi)
                        cv2.waitKey(0)
                        cv2.destroyWindow("ROI")
                        # Save image
                        cv2.imwrite(os.path.join(save_dir, 'fixedWing' + str(i+1) + str(k) + '.png'), roi)

            # if the 'p' key is pressed, pass next image at directory.
            elif key == ord("p"):
                break

            # if the 'q' key is pressed, quit image.
            elif key == ord("q"):
                exit()

        # close all open windows
        cv2.destroyAllWindows()
